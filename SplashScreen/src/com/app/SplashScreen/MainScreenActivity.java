package com.app.SplashScreen;

import android.app.Activity;
import android.os.Bundle;

public class MainScreenActivity extends Activity {
    /**
     * Empty class for testing the new Splash Screen. Replace this class with the main activity of your app
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
    }
}