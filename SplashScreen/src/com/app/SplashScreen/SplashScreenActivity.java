package com.app.SplashScreen;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;


/**
 * Created by timur on 14/03/2014.
 */

/**
 * Generic Splash Screen activity. Uses simple countdown in milliseconds (ms).
 * Check final int introScreenDisplayTime to change the time
 * Check drawable-hdpi for uea logo file (drawable-hdpi/uealogo1.png)
 */

public class SplashScreenActivity extends Activity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.intro);

        /** Setting time (How long the intro screen should be displayed) */

        final int introScreenDisplayTime = 3500;

        /** Starting a new thread - it is active until time we set runs out */

        Thread UEAIntroThread = new Thread() {

            int timer = 0;

            @Override
            public void run() {
                try {
                    super.run();

                    while (timer < introScreenDisplayTime) {
                        sleep(100);
                        timer += 100;
                    }
                } catch (Exception e) {
                    System.out.println("Intro Screen Activity exception caught:" + e);
                } finally {

                    /** switching to the next Activity (in our case - MainScreenActivity) */

                    startActivity(new Intent(SplashScreenActivity.this,
                            MainScreenActivity.class));
                    finish();
                }
            }
        };
        UEAIntroThread.start();

    }
}

