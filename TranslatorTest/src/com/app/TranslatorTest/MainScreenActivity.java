package com.app.TranslatorTest;

/**
 * Created by timur on 20/03/2014.
 */

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.memetix.mst.language.Language; // imports for Microsoft Translator API
import com.memetix.mst.translate.Translate; //Make sure to import it to your project before running the app


/*
 * Very basic app - it just passes the string from EditText form to
 * Microsoft Servers via MS Translator API. All setting are hardcoded at the moment -
 * So it only translates from English to French.
 * API settings (App key, app password) are also defined in this class
 * There were some issues with connecting to the MS Server via virtual device for some reason,
 * So it only works from a real gadget.
 */

public class MainScreenActivity extends Activity  {


    public String translate(String text) throws Exception {

        Translate.setClientId("ueatranslate2014"); //uea translate app ID.
        Translate.setClientSecret("GxWb8xjY3ujvD8ggSStbsdXw0yXWROnnkWlCE1LnSAk="); // uea translate app secret key
        // if for some reason you need to change these settings - please contact me (Timur)


        String translated = "";

        // French is hardcoded
        translated = Translate.execute(text,Language.FRENCH);
        // Although Microsoft API supports most of widespread languages,
        // character sets for languages with non-latin alphabets may cause some issues..

        return translated;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        Button translateButton = (Button) findViewById(R.id.buttonTranslate);
        translateButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

             /* Creating Async Task - It is s used to make translation process independent from the user interface,
             In case if translation takes too long (poor connection, many words in input, etc..)
              */

                class translationAsyncTask extends AsyncTask<Void, Void, Void> {

                    String translated = "";

                    @Override

                    /* doInBackground - It will do the translation using an independent thread */

                    protected Void doInBackground(Void... arg0) {

                        String enteredText = ((EditText) findViewById(R.id.edittextEnterText)).getText().toString();
                        try {
                            translated = translate(enteredText);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        return null;
                    }

                    @Override

                     /* onPostExecute - Runs on the UI thread after translation is completed */

                    protected void onPostExecute(Void result) {
                        ((TextView) findViewById(R.id.textviewTranslatedText)).setText(translated);
                        super.onPostExecute(result);
                    }

                }

                new translationAsyncTask().execute();
            }
        });
    }

}