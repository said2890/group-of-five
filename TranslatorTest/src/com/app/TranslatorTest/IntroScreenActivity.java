package com.app.TranslatorTest;

/**
 * Created by timur on 14/03/2014.
 */

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;


public class IntroScreenActivity extends Activity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.intro);

        /* Setting time (How long the welcome screen should be displayed) */

        final int introScreenDisplayTime = 3500;

        /* Starting a new thread - it is active until time we set runs out */

        Thread UEAIntroThread = new Thread() {

            int timer = 0;

            @Override
            public void run() {
                try {
                    super.run();

                    while (timer < introScreenDisplayTime) {
                        sleep(100);
                        timer += 100;
                    }
                } catch (Exception e) {
                    System.out.println("EXc=" + e);
                } finally {

                    /** switching to the next Activity (in our case - MainScreenActivity) */

                    startActivity(new Intent(IntroScreenActivity.this,
                            MainScreenActivity.class));
                    finish();
                }
            }
        };
        UEAIntroThread.start();

    }
}
