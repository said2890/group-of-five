
 Very basic app - it just passes the string from EditText form to Microsoft Servers via MS Translator API.
 All setting are hardcoded at the moment, so it only translates from English to French.
 API settings (App key, app password) are defined in the Main Activity.
 There were some issues with connecting to the MS Server via virtual device,

If you have problems with opening/running the project, it is most likely that:
1) Microsoft API Libraries are missing (google them or text me - I'll send you the jar file)
2) There were some issues with exporting project to Eclipse/ADT (I am using IJ IDEA as the main IDE)
3) You are testing it on virtual device - try gadget instead
4) Java SDK Version is not correct (I am using Android SDK level 13 (3.2) in this project)

I am currently working on:
1) Language Detection
2) Translation to selected language
3) Language Settings for the Main Screen
3) Voice processing - still not a priority :)

Timur, 23/03/14