package com.sofia.framework;

import info.androidhive.slidingmenu.R;
import android.annotation.SuppressLint;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class DrawerActivity extends FragmentActivity{
	
	private DrawerLayout mDrawerLayout;
	private ListView mDrawerList;
	private ActionBarDrawerToggle mDrawerToggle;

	// nav drawer title
	private CharSequence mDrawerTitle;

	private int layoutfile = R.layout.activity_main;
	private String TAG = "Main";
	
	private CharSequence mTitle;

	private ItemAdapter adapter;
	private ItemArray locArray;
	private GoogleMap gm;
	
	private Vibrator mVib;

	@SuppressWarnings("unchecked")
	@SuppressLint("NewApi")
	
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//setContentView(R.layout.activity_main);
		setContentView(layoutfile);

		mTitle = mDrawerTitle = getTitle();

		// load slide menu items
		// navMenuTitles =
		// getResources().getStringArray(R.array.nav_drawer_items);
		
		
		//vibrates the phone
		mVib = (Vibrator) this.getSystemService(VIBRATOR_SERVICE);
		
		locArray = new ItemArray();
		//locArray.removeAll();
		LatLng latlng = new LatLng(52.621762999999990000, 1.240993000000003100);

		locArray.addItemWithObject("Somewhere", latlng);
		locArray.addItemWithObject("Over", latlng);
		locArray.addItemWithObject("The", latlng);
		locArray.addItemWithObject("Rainbow", latlng);
		locArray.addItemWithObject("Pusheen", latlng);
		locArray.addItemWithObject("Roams...", latlng);
		
		Log.e(TAG, locArray.getTitle(3));


		// the
		int one = R.drawable.bullet_point;

		// both of these ids are within the activity_main.xml
		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		mDrawerList = (ListView) findViewById(R.id.list_slidermenu);

		

		

		mDrawerList.setOnItemClickListener(new SlideMenuClickListener());

		// setting the nav drawer list adapter
		adapter = new ItemAdapter(getApplicationContext(), locArray, R.layout.drawer_list_item2);
		mDrawerList.setAdapter(adapter);

		// enabling action bar app icon and behaving it as toggle button
		getActionBar().setDisplayHomeAsUpEnabled(true);
		getActionBar().setHomeButtonEnabled(true);

		mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
				R.drawable.ic_drawer, // nav menu toggle icon
				R.string.app_name, // nav drawer open - description for
				// accessibility
				R.string.app_name // nav drawer close - description for
		// accessibility
		) {
			public void onDrawerClosed(View view) {
				getActionBar().setTitle(mTitle);
				// calling onPrepareOptionsMenu() to show action bar icons
				invalidateOptionsMenu();
			}

			public void onDrawerOpened(View drawerView) {
				getActionBar().setTitle(mDrawerTitle);
				// calling onPrepareOptionsMenu() to hide action bar icons
				invalidateOptionsMenu();
			}
		};
		mDrawerLayout.setDrawerListener(mDrawerToggle);
		getMap();

		// if (savedInstanceState == null) {
		// on first time display view for first nav item (homefragment)
		// refers to method below which appears to control everything!

		// displayView(0);
		// }
	}

	/**
	 * Slide menu item click listener
	 * */
	private class SlideMenuClickListener
			implements
				ListView.OnItemClickListener {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
				long id) {
			// display view for selected nav drawer item
			// refers to the method below
			displayView(position);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// toggle nav drawer on selecting action bar app icon/title
		if (mDrawerToggle.onOptionsItemSelected(item)) {
			return true;
		}
		// Handle action bar actions click
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			
				return true;
		}
		else {
				return super.onOptionsItemSelected(item);
		}
	}

	/* *
	 * Called when invalidateOptionsMenu() is triggered
	 */
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		// if nav drawer is opened, hide the action items
		boolean drawerOpen = mDrawerLayout.isDrawerOpen(mDrawerList);
		menu.findItem(R.id.action_settings).setVisible(!drawerOpen);
		return super.onPrepareOptionsMenu(menu);
	}

	/**
	 * Displaying fragment view for selected nav drawer list item.
	 * Takes the position and uses that information to get the information
	 * to centre the camera correctly etc...
	 * 
	 * */
	private void displayView(int position) {
		Log.e(TAG, "displayView");
		
		Toast.makeText(getApplicationContext(),
				"Pressed Something", Toast.LENGTH_SHORT).show();
		mVib.vibrate(50);
		
		boolean marker = false;

		//I don't even want to go into what this bit of code does...
		//but here goes...
		//it gets the latlng coordinates for sepcified position and moves the 
		//map to that location
		//unless there's no latlng
		//in which case just displays message..ok?
		if (locArray.getObject(position) != null){
		if (position == locArray.getPosition(position)) {
			if (marker = true) {
				gm.clear();
			}
			placeMarker((LatLng) locArray.getObject(position),
					locArray.getTitle(position));
			marker = true;
			
			setUpDrawer(position);
		}
		} else {
			Toast.makeText(getApplicationContext(),
					"No location!", Toast.LENGTH_SHORT).show();
		}
		setTitle(locArray.getTitle(position));
	}

	

	/**
	 * When using the ActionBarDrawerToggle, you must call it during
	 * onPostCreate() and onConfigurationChanged()...
	 */

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		// Sync the toggle state after onRestoreInstanceState has occurred.
		mDrawerToggle.syncState();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		// Pass any configuration change to the drawer toggls
		mDrawerToggle.onConfigurationChanged(newConfig);
	}

	/**
	 * Initilises the map by loaded the fragment.
	 */
	private void initilizeMap() {
		if (gm == null) {
			Log.e(TAG, "gm null");

			gm = ((SupportMapFragment) getSupportFragmentManager()
					.findFragmentById(R.id.map)).getMap();

			// check if map is created successfully or not
			if (gm == null) {
				Toast.makeText(getApplicationContext(),
						"Sorry! unable to create maps", Toast.LENGTH_SHORT)
						.show();
			}
		}

	}

	/**
	 * Private method to get the map. Calls on the initiliseMap method to laod
	 * the map
	 */
	private void getMap() {
		try {
			Log.e(TAG, "getMap");
			// Loading map
			initilizeMap();

		} catch (Exception e) {
			e.printStackTrace();

		}

	}

	/**
	 * Takes the coordinates and places the view of the map on that area.
	 * Animates the camera. (It's really cool).
	 * 
	 * @param latlng
	 *            LatLng
	 * @param name
	 *            String
	 */
	private void placeMarker(LatLng latlng, String name) {

		gm.addMarker(new MarkerOptions().position(latlng).title(name));
		CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latlng,
				10);
		gm.animateCamera(cameraUpdate);

	}

	/**
	 * Closes the sliding menu and sets the title as being the title of the
	 * location
	 * 
	 * @param position
	 */
	private void setUpDrawer(int position) {
		setTitle(toString(position));
		mDrawerLayout.closeDrawer(mDrawerList);
		

	}
	/**
	 * Converts String name to char
	 * @param position
	 * @return
	 */
	  private CharSequence toString (int position) {
			Log.e(TAG, "toString Char" + locArray.getTitle(position));
		  CharSequence title = locArray.getTitle(position);
			return title;


	  }
	  

	  
	
}


