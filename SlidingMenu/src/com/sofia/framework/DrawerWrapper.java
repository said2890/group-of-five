package com.sofia.framework;
/**
 * This Class acts as a wrapper for the DrawerActivity class.
 * @author sofiaprice
 *
 */
public class DrawerWrapper {

	private DrawerActivity drawerAct;
	
	/**
	 * Constructor containing the instantiation of the DrawerActivity.
	 */
	public DrawerWrapper(){
		drawerAct = new DrawerActivity();
		//drawerAct.selectLayout(layout);
	}
	
	
}
