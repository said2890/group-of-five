package com.sofia.framework;

/**
 * This class creates an item that populates ItemArray, and is used by ItemAdapter. Contains
 * the getter and setters for this information  
 * @author sofiaprice
 *
 */
public class Item {
	
	private String title;
	private int icon;
	private Object obj;

	/**
	 * Default constructor
	 */
	public Item() {
	}

	/**
	 * Constructor if you want item to have both title and custom icon
	 * @param title
	 * @param icon
	 */
	public Item(String title, int icon) {
		this.title = title;
		this.icon = icon;
	}

	/**
	 * Constructor if you want item to have only a string title
	 * @param title as String
	 */
	public Item(String title) {
		this.title = title;
	}
	
	public Item(String title, Object obj){
		this.title = title;
		this.obj = obj;
	}

	/**
	 * Returns the title of the object as a String. 
	 * @return title as String
	 */
	public String getTitle() {
		return this.title;
	}

	/**
	 * Returns the int value that can be used to assign a custom icon
	 * @return icon as integer
	 */
	public int getIcon() {
		return this.icon;
	}

	

	/**
	 * Set the title of the Item. 
	 * @param title String
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * Set the interger value for the resource to use as the icon
	 * @param icon int
	 */
	public void setIcon(int icon) {
		this.icon = icon;
	}

	public void setObject(Object obj){
		this.obj = obj;
	}
	
	public Object getObject(int position){
		return this.obj;
		
	}


}


