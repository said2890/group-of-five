package com.sofia.framework;

public abstract class AbstractFeedback {

	
	public abstract void haptic();
	
	public abstract void sound();
	
	public abstract void soundAndHaptic();
	
}
