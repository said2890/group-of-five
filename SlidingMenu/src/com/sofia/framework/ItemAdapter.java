package com.sofia.framework;
import info.androidhive.slidingmenu.R;
import info.androidhive.slidingmenu.model.LocationArray;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * This class can be used to convert an array list into the data to fill a
 * ListView
 * 
 * @author sofiaprice
 * 
 */
public class ItemAdapter extends BaseAdapter {

	private Context context;
	private ItemArray itemArray;
	private int Layout;

	/**
	 * Constructor for the ItemAdaptor. Requires input of the context, an ItemArray object, and the
	 * layout xml file to be used as the design for the list. 
	 * @param context Context
	 * @param itemArray ItemArray
	 * @param Layout integer
	 */
	public ItemAdapter(Context context, ItemArray itemArray, int Layout) {
		this.context = context;
		this.itemArray = itemArray;
		this.Layout = Layout;
	}

	
	@Override
	public int getCount() {
		return itemArray.getSize();
	}

	@Override
	public Object getItem(int position) {
		return itemArray.getObject(position);

	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			LayoutInflater mInflater = (LayoutInflater) context
					.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
			convertView = mInflater.inflate(Layout, null);
		}

		ImageView imgIcon = (ImageView) convertView.findViewById(R.id.icon);
		TextView txtTitle = (TextView) convertView.findViewById(R.id.title);

		txtTitle.setText(itemArray.getTitle(position));

		return convertView;
	}
	

}
