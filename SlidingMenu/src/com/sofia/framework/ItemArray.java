package com.sofia.framework;

import java.util.ArrayList;

public class ItemArray {
	
	private Item item;
	private ArrayList <Item> itemArray;
	
	public ItemArray(){
		itemArray = new ArrayList<Item>();
	}
	

	/**
	 * returns a string value for the title of that location.Input the position in
	 * the array you wish to remove the title from. 
	 * @param position
	 * @return
	 */
	public String getTitle(int position){
		
		item = itemArray.get(position);
		String itemName = item.getTitle();
		return itemName;
		
	}
	
	/**
	 * returns the latlng object for the postion that was input
	 * @param position
	 * @return Latlng
	 */

	
	/**
	 * add a location to location constructir as only String. This is not really supposed
	 * to be used... but I'll leave here for now!
	 * @param name
	 */
	public void addItemOnlyName(String name) {
		item = new Item(name);
	}
	
	/**
	 * Construct a new UEALocation object with a name String and an icon integer value 
	 * @param name
	 * @param latlng
	 */
	public void addItemWithIcon(String name, int icon){
		item = new Item(name, icon);
		itemArray.add(item);
	}
	
	public void addItemWithObject (String name, Object obj){
		item = new Item(name, obj);
		itemArray.add(item);
	}
	/**
	 * Gets the size of the array as an integer value
	 * @return integer
	 */
	public int getSize(){
		return itemArray.size();
	}
	
	public Object getObject(int position){
		Object obj = itemArray.get(position).getObject(position);
		return obj;
	}
	/**
	 * Gets the position of the item as an interger
	 * @param position
	 * @return integer
	 */
	
	public int getPosition(int position) {
		itemArray.get(position);
		return position;
		
	}


	/**
	 * gets the Object at the specified position in the Array.
	 * @param position
	 * @return Array Object
	 */
	public Object getItem(int position) {
		Object obj = itemArray.get(position);
		return obj;
	}
	
	public void removeAll(){
		itemArray.clear();
	}
}

