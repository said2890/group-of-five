package Maps;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

public interface Map {

    public abstract GoogleMap getMap(int fragmentToInflate);
    
    public abstract void trackUser();

    public abstract Marker placeMarker(double latitude, double longitude, String name);
    
    public abstract MarkerOptions markerO(double latitude, double longitude, String name);
    
    public abstract void clearMap();
}
