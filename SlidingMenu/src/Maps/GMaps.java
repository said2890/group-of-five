package Maps;

import info.androidhive.slidingmenu.R;
import android.util.Log;
import android.view.View;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;

public class GMaps extends SupportMapFragment {
	 
	public static String TAG = "GMaps";
	public GMaps() {
	    }

	    /**
	     * Returns a new Google Map in the specified fragment from the R file.
	     */
	    public GoogleMap getMap(GoogleMap map) {
		try {
		    // Loading map
		    Log.e(TAG, "GET1");
		    map = initilizeMap( map);
		    return map;

		} catch (Exception e) {
		    Log.e(TAG, "GETmAP");
		    e.printStackTrace();

		    return null;
		}
	    }

	    /**
	     * Initialises map using the Google Maps API. If Google Maps cannot be
	     * loaded a Toast message is displayed to the user.
	     * 
	     * @param fragmentToInflate
	     */
	    public GoogleMap initilizeMap(GoogleMap gm) {

		Log.e(TAG, "smfjj");
		gm = ((SupportMapFragment) getFragmentManager()
			.findFragmentById(R.id.map)).getMap();
		Log.e(TAG, "smf");
		return gm;
		// check if map is created successfully or not

	    }
}
