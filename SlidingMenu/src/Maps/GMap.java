package Maps;

import info.androidhive.slidingmenu.R;
import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

public class GMap extends FragmentActivity {

    // private GoogleMap gm;
    private Marker marker;
    private MarkerOptions markerO;
    private String TAG = "GMap";

    /**
     * Default constructor
     */
    public GMap(Context context) {
    }

    public GMap(){
    	
    }
    /**
     * Returns a new Google Map in the specified fragment from the R file.
     */
    public GoogleMap getMap(GoogleMap map) {
	try {
	    // Loading map
	    Log.e(TAG, "getMap");
	    map = initilizeMap( map);
	    return map;

	} catch (Exception e) {
	    Log.e(TAG, "GETmAP");
	    e.printStackTrace();

	    return null;
	}
    }

    /**
     * Initialises map using the Google Maps API. If Google Maps cannot be
     * loaded a Toast message is displayed to the user.
     * 
     * @param fragmentToInflate
     */
    public GoogleMap initilizeMap(GoogleMap gm) {

	Log.e(TAG, "before inti");
	gm = ((SupportMapFragment) getSupportFragmentManager()
		.findFragmentById(R.id.map)).getMap();
	Log.e(TAG, "after init");
	return gm;
	// check if map is created successfully or not

    }

    public void placeMarker(GoogleMap mGMap, double latitude, double longitude,
	    String name, boolean moveCamera) {

	mGMap.addMarker(new MarkerOptions().position(
		new LatLng(latitude, longitude)).title(name));
	if (moveCamera == true) {
	    CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(
		    new LatLng(latitude, longitude), 10);
	    mGMap.animateCamera(cameraUpdate);
	}
    }



    public void clearMap(GoogleMap gm) {
	gm.clear();
    }

    public void findMe(GoogleMap gm){
	///need to add check for gps enabled
	gm.setMyLocationEnabled(true);
    }
}
