package info.androidhive.slidingmenu;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.LatLng;

public class MapFrag extends Fragment {

    private MapView mapView;
    private GoogleMap map;
    private String LOG = "mapfrag";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
	    Bundle savedInstanceState) {
	Log.e(LOG, "before vuew");
	View v = inflater.inflate(R.layout.map_fragment, container, false);
	// Gets the MapView from the XML layout and creates it
	Log.e(LOG, "after vuew");
			mapView = (MapView) v.findViewById(R.id.google_map);
			mapView.onCreate(savedInstanceState);
	 
			// Gets to GoogleMap from the MapView and does initialization stuff
			map = mapView.getMap();
			map.getUiSettings().setMyLocationButtonEnabled(false);
			map.setMyLocationEnabled(true);
	 
			// Needs to call MapsInitializer before doing any CameraUpdateFactory calls
			
			
			// Updates the location and zoom of the MapView
			//CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(new LatLng(43.1, -87.9), 10);
			//map.animateCamera(cameraUpdate);
			Log.w(LOG, "after all");
			return v;
		}

    @SuppressLint("NewApi")
	@Override
    public void onResume() {
	super.onResume();
	mapView.onResume();
    }

    public void placeMarker(double Latitude, double Longitide) {
	CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(new LatLng(Latitude, Longitide), 10);
	map.animateCamera(cameraUpdate);
    }
    @Override
    public void onPause() {
	super.onPause();
	mapView.onPause();
    }

    @Override
    public void onDestroy() {
	super.onDestroy();
	mapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
	super.onLowMemory();
	mapView.onLowMemory();
    }
}
