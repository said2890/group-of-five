package info.androidhive.slidingmenu.adapter;

import info.androidhive.slidingmenu.R;
import info.androidhive.slidingmenu.model.LocationArray;
import info.androidhive.slidingmenu.model.UEALocation;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class UEALocationAdapter extends BaseAdapter  {
   
	private Context context;
    private ArrayList<UEALocation> uealocation;
    
    private LocationArray locArray;

    public UEALocationAdapter(Context context,
	    ArrayList<UEALocation> uealocation) {
	this.context = context;
	this.uealocation = uealocation;
    }

    @Override
    public int getCount() {
	return uealocation.size();
    }

    @Override
    public Object getItem(int position) {
	return uealocation.get(position);
    }

    @Override
    public long getItemId(int position) {
	return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
	if (convertView == null) {
	    LayoutInflater mInflater = (LayoutInflater) context
		    .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
	    convertView = mInflater.inflate(R.layout.drawer_list_item2, null);
	}

	ImageView imgIcon = (ImageView) convertView.findViewById(R.id.icon);
	TextView txtTitle = (TextView) convertView.findViewById(R.id.title);
	

	imgIcon.setImageResource(uealocation.get(position).getIcon());
	// gets title from Strings XML array
	txtTitle.setText(uealocation.get(position).getTitle());

	return convertView;
    }
}
