package info.androidhive.slidingmenu.adapter;

import info.androidhive.slidingmenu.R;
import info.androidhive.slidingmenu.model.LocationArray;
import info.androidhive.slidingmenu.model.UEALocation;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class UEALocationAdapter2 extends BaseAdapter {

	private static String LOG = "LOG";
	private Context context;

	private LocationArray locArray;
	
	private int Layout;

	public UEALocationAdapter2(Context context, LocationArray locArray, int Layout) {
		this.context = context;
		this.locArray = locArray;
		this.Layout = Layout;
	}

	@Override
	public int getCount() {
		return locArray.getSize();
	}

	@Override
	public Object getItem(int position) {
		return locArray.getObject(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			LayoutInflater mInflater = (LayoutInflater) context
					.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
			convertView = mInflater.inflate(Layout, null);
		}

		ImageView imgIcon = (ImageView) convertView.findViewById(R.id.icon);
		TextView txtTitle = (TextView) convertView.findViewById(R.id.title);
		
		 //imgIcon.setImageResource(navDrawerItems.get(position).getIcon());

		txtTitle.setText(locArray.getTitles(position).toString());
		Log.e(LOG, locArray.getTitles(position).toString());
		//imgIcon.setImageResource(uealocation.get(position).getIcon());
		// gets title from Strings XML array
		//txtTitle.setText(uealocation.get(position).getTitle());

		return convertView;
	}

	public int getLayout(int Layout) {
		return Layout;
	}
}
