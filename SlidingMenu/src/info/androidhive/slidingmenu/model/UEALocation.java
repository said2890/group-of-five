package info.androidhive.slidingmenu.model;

import com.google.android.gms.maps.model.LatLng;
import com.sofia.framework.Item;

public class UEALocation {

	private String title;
	private int icon;
	private LatLng latlng;

	public UEALocation() {
	}

	public UEALocation(String title, int icon) {
		this.title = title;
		this.icon = icon;
	}

	public UEALocation(String title) {
		this.title = title;
	}

	public UEALocation(String title, LatLng latlng) {
		this.latlng = latlng;
		this.title = title;

	}

	public String getTitle() {
		return this.title;
	}

	public int getIcon() {
		return this.icon;
	}

	/**
	 * need to chang e to this.latlng????
	 * 
	 * @param latitude
	 * @param longitude
	 * @return
	 */
	public LatLng setLatLng(double latitude, double longitude) {
		latlng = new LatLng(latitude, longitude);
		return latlng;

	}

	public LatLng getLatLng() {
		return this.latlng;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setIcon(int icon) {
		this.icon = icon;
	}

}
