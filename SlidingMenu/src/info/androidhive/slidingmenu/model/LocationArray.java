package info.androidhive.slidingmenu.model;

import java.util.ArrayList;

import com.google.android.gms.maps.model.LatLng;
import com.sofia.framework.Item;

public class LocationArray {

	private UEALocation2 location;
	private ArrayList<UEALocation2> locArray;

	public LocationArray() {
		locArray = new ArrayList<UEALocation2>();
	}

	/**
	 * reutrns a string value for the title of that location.Input the position
	 * in the array you wish to remove the title from.
	 * 
	 * @param position
	 * @return
	 */
	public String getTitle(int position) {

		location = locArray.get(position);
		String locationName = location.getTitle();
		return locationName;

	}

	/**
	 * Returns the object at the given positon from the ArrayList
	 * 
	 * @param position
	 *            integer
	 * @return Object
	 */
	public Object getObject(int position) {
		Object obj = locArray.get(position);
		return obj;
	}

	/**
	 * returns the latlng object for the postion that was input
	 * 
	 * @param position
	 * @return Latlng
	 */
	public LatLng getLatLng(int position) {
		location = locArray.get(position);
		LatLng latlng = location.getLatLng();
		return latlng;
	}

	/**
	 * add a location to location constructir as only String. This is not really
	 * supposed to be used... but I'll leave here for now! --- should remove...
	 * :P
	 * 
	 * @param name
	 */
	public void addLocationName(String name) {
		location = new UEALocation2(name);
	}
	
	public void addItemWithIcon(String name, int icon){
		location = new UEALocation2(name, icon);
		locArray.add(location);
	}
	/**
	 * if the array of items size is greater than 0, it will return true.
	 * 
	 * @return
	 */
	public boolean hasNext() {

		if (locArray.size() == 0) {
			return false;
		}
		return true;
	}
	/**
	 * Gets the position of the item as an interger
	 * @param position
	 * @return
	 */
	
	public String getTitles(int position){
		String title = locArray.get(position).getTitle();
		return title;
	}
	
	public int getPosition(int position) {
		locArray.get(position);
		return position;
		
	}

	/**
	 * Construct a new UEALocation object with a latlng and a string value
	 * 
	 * @param name
	 * @param latlng
	 */
	public void addLocation(String name, Object latlng) {
		location = new UEALocation2(name, latlng);
		locArray.add(location);
	}

	public int getSize() {
		return locArray.size();
	}

	private class UEALocation2 {

		private String title;
		private int icon;
		private LatLng latlng;

		public UEALocation2() {
		}

		public UEALocation2(String title, int icon) {
			this.title = title;
			this.icon = icon;
		}

		public UEALocation2(String title) {
			this.title = title;
		}

		public UEALocation2(String title, Object latlng) {
			this.latlng = (LatLng) latlng;
			this.title = title;

		}

		public String getTitle() {
			return this.title;
		}

		public int getIcon() {
			return this.icon;
		}

		/**
		 * need to chang e to this.latlng????
		 * 
		 * @param latitude
		 * @param longitude
		 * @return
		 */
		public LatLng setLatLng(double latitude, double longitude) {
			latlng = new LatLng(latitude, longitude);
			return latlng;

		}

		public LatLng getLatLng() {
			return this.latlng;
		}

		public void setTitle(String title) {
			this.title = title;
		}

		public void setIcon(int icon) {
			this.icon = icon;
		}

		/**
		 * gets the Object at the specified position in the Array.
		 * @param position
		 * @return
		 */
		public Object getObject(int position) {
			Object obj = locArray.get(position);
			return obj;
		}
	}

}
