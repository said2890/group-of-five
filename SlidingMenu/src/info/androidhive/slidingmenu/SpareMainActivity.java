package info.androidhive.slidingmenu;


import info.androidhive.slidingmenu.adapter.UEALocationAdapter;

import info.androidhive.slidingmenu.model.UEALocation;

import java.util.ArrayList;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;


import Maps.GMap;
import Maps.Map;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

public class SpareMainActivity extends FragmentActivity {
    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private ActionBarDrawerToggle mDrawerToggle;

    // nav drawer title
    private CharSequence mDrawerTitle;
    private GMap m;
    private String TAG = "Main";
    // used to store app title
    private CharSequence mTitle;

    
    // slide menu items
    private String[] navMenuTitles;
    private TypedArray navMenuIcons;
    
    private ArrayList<UEALocation> uealocation;
    
   
    private UEALocationAdapter adapter;

    GoogleMap gm;
    
    private Map map; 

    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	setContentView(R.layout.activity_main);

	mTitle = mDrawerTitle = getTitle();

	// load slide menu items
	navMenuTitles = getResources().getStringArray(R.array.nav_drawer_items);

	// nav drawer icons from resources
	navMenuIcons = getResources()
		.obtainTypedArray(R.array.nav_drawer_icons);

	//both of these ids are within the activity_main.xml
	mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
	mDrawerList = (ListView) findViewById(R.id.list_slidermenu);

	uealocation = new ArrayList<UEALocation>();
	

	// adding nav drawer items to array
	// Home
	// navMenuIcons refers to the array list reference in the xml Strings
	// file
	
	uealocation.add(new UEALocation(navMenuTitles[0], navMenuIcons
		.getResourceId(0, -1)));
	// Find People
	uealocation.add(new UEALocation(navMenuTitles[1], navMenuIcons
		.getResourceId(1, -1)));
	// Photos
	uealocation.add(new UEALocation(navMenuTitles[2], navMenuIcons
		.getResourceId(2, -1)));
	// Communities, Will add a counter here
	uealocation.add(new UEALocation(navMenuTitles[3], navMenuIcons
		.getResourceId(3, -1)));
	// Pages
	uealocation.add(new UEALocation(navMenuTitles[4], navMenuIcons
		.getResourceId(4, -1)));
	// What's hot, We will add a counter here
	uealocation.add(new UEALocation(navMenuTitles[5], navMenuIcons
		.getResourceId(5, -1) ));

	// Recycle the typed array
	navMenuIcons.recycle();

	mDrawerList.setOnItemClickListener(new SlideMenuClickListener());

	// setting the nav drawer list adapter
	adapter = new UEALocationAdapter(getApplicationContext(),
		uealocation);
	mDrawerList.setAdapter(adapter);

	// enabling action bar app icon and behaving it as toggle button
	getActionBar().setDisplayHomeAsUpEnabled(true);
	getActionBar().setHomeButtonEnabled(true);

	mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
		R.drawable.ic_drawer, // nav menu toggle icon
		R.string.app_name, // nav drawer open - description for
				   // accessibility
		R.string.app_name // nav drawer close - description for
				  // accessibility
	) {
	    public void onDrawerClosed(View view) {
		getActionBar().setTitle(mTitle);
		// calling onPrepareOptionsMenu() to show action bar icons
		invalidateOptionsMenu();
	    }

	    public void onDrawerOpened(View drawerView) {
		getActionBar().setTitle(mDrawerTitle);
		// calling onPrepareOptionsMenu() to hide action bar icons
		invalidateOptionsMenu();
	    }
	};
	mDrawerLayout.setDrawerListener(mDrawerToggle);

	if (savedInstanceState == null) {
	    // on first time display view for first nav item (homefragment)
	    // refers to method below which appears to control everything!

	   displayView(0);
	}
    }

    /**
     * Slide menu item click listener
     * */
    private class SlideMenuClickListener implements
	    ListView.OnItemClickListener {
	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
		long id) {
	    // display view for selected nav drawer item
	    // refers to the method below
	    displayView(position);
	}
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
	getMenuInflater().inflate(R.menu.main, menu);
	return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
	// toggle nav drawer on selecting action bar app icon/title
	if (mDrawerToggle.onOptionsItemSelected(item)) {
	    return true;
	}
	// Handle action bar actions click
	int id = item.getItemId();
	if (id == R.id.action_settings) {
		
			return true;
	}
	else {
			return super.onOptionsItemSelected(item);
	}
}

    /* *
     * Called when invalidateOptionsMenu() is triggered
     */
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
	// if nav drawer is opened, hide the action items
	boolean drawerOpen = mDrawerLayout.isDrawerOpen(mDrawerList);
	menu.findItem(R.id.action_settings).setVisible(!drawerOpen);
	return super.onPrepareOptionsMenu(menu);
    }

    /**
     * Diplaying fragment view for selected nav drawer list item
     * */
    private void displayView(int position) {
	Log.e(TAG, "displayView");
	Toast.makeText(getApplicationContext(), "YOU PRESSED SOMETHING LOL ROFL", Toast.LENGTH_SHORT).show();
	
	//getMap();

	@SuppressWarnings("unused")
	
	MapFrag act = null;
	//act = new MapFrag();
	boolean marker = false;
	switch (position) {
	
	case 0:
	   Log.e(TAG, "case o");
	    act = new MapFrag();
	    act.placeMarker(52.621762999999990000,1.240993000000003100);
	 /* if (marker = true) {
	      gm.clear();
	  } 
	  
	    gm.addMarker(new MarkerOptions().position(
		    new LatLng(52.621762999999990000, 1.240993000000003100))
		    .title("UEA"));
	    marker = true;
	    mDrawerLayout.closeDrawer(mDrawerList);
	    setTitle(navMenuTitles[position]);
	    */
	    break;
	
	case 1:
	    act = new MapFrag();
	    ((MapFrag) act).placeMarker(52.621762999999990000,1.240993000000003100);
	    mDrawerLayout.closeDrawer(mDrawerList);
	    setTitle(navMenuTitles[position]);
	    break;
	
	case 2:
	    if (marker = true) {
		      gm.clear();
		  } 
		    gm.addMarker(new MarkerOptions().position(
			    new LatLng(52.621762999999990000, 1.240993000000003100))
			    .title("UEA"));
		    marker = true;
		    mDrawerLayout.closeDrawer(mDrawerList);
		    setTitle(navMenuTitles[position]);
	    break;
	case 3:
	    if (marker = true) {
		      gm.clear();
		  } 
		    gm.addMarker(new MarkerOptions().position(
			    new LatLng(50.621762999999990000, 1.240993000000003100))
			    .title("UEA"));
		    marker = true;
		    mDrawerLayout.closeDrawer(mDrawerList);
		    setTitle(navMenuTitles[position]);
	    break;
	case 4:
	    if (marker = true) {
		      gm.clear();
		  } 
		    gm.addMarker(new MarkerOptions().position(
			    new LatLng(54.621762999999990000, 1.240993000000003100))
			    .title("UEA"));
		    marker = true;
		    mDrawerLayout.closeDrawer(mDrawerList);
		    setTitle(navMenuTitles[position]);
	    break;
	case 5:
	    if (marker = true) {
		      gm.clear();
		  } 
		    gm.addMarker(new MarkerOptions().position(
			    new LatLng(57.621762999999990000, 1.240993000000003100))
			    .title("UEA"));
		    marker = true;
		    mDrawerLayout.closeDrawer(mDrawerList);
		    setTitle(navMenuTitles[position]);
	    break;

	default:
	    break;
	} if (act != null) {
            FragmentManager fragmentManager = getFragmentManager();
            fragmentManager.beginTransaction()
                    .replace(R.id.frame_container, act).commit();
 
            // update selected item and title, then close the drawer
            mDrawerList.setItemChecked(position, true);
            mDrawerList.setSelection(position);
            setTitle(navMenuTitles[position]);
            mDrawerLayout.closeDrawer(mDrawerList);
        } else {
            // error in creating fragment
            Log.e("MainActivity", "Error in creating fragment");
        }

	
    }

    @Override
    public void setTitle(CharSequence title) {
	mTitle = title;
	getActionBar().setTitle(mTitle);
    }

    /**
     * When using the ActionBarDrawerToggle, you must call it during
     * onPostCreate() and onConfigurationChanged()...
     */

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
	super.onPostCreate(savedInstanceState);
	// Sync the toggle state after onRestoreInstanceState has occurred.
	mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
	super.onConfigurationChanged(newConfig);
	// Pass any configuration change to the drawer toggls
	mDrawerToggle.onConfigurationChanged(newConfig);
    }
    
    
    private void initilizeMap() {
	if (gm == null) {
	    Log.e(TAG, "gm null");

		//gm = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(
		//		R.id.map)).getMap();

		// check if map is created successfully or not
		if (gm == null) {
			Toast.makeText(getApplicationContext(),
					"Sorry! unable to create maps", Toast.LENGTH_SHORT)
					.show();
		}
	}
	
    }
    
    private void getMap() {
	try {
		// Loading map
		initilizeMap();
		

	} catch (Exception e) {
		e.printStackTrace();
		
	}
	
    }
}


