package info.androidhive.slidingmenu;

import info.androidhive.slidingmenu.adapter.UEALocationAdapter;
import info.androidhive.slidingmenu.adapter.UEALocationAdapter2;
import info.androidhive.slidingmenu.model.LocationArray;
import info.androidhive.slidingmenu.model.UEALocation;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import Maps.GMap;
import Maps.Map;
import android.annotation.SuppressLint;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.location.LocationClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.sofia.framework.ItemAdapter;
import com.sofia.framework.ItemArray;

public class SpareMainActivity4 extends FragmentActivity {
    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private ActionBarDrawerToggle mDrawerToggle;

    // nav drawer title
    private CharSequence mDrawerTitle;
    private GMap m;
    private String TAG = "Main";
    // used to store app title
    private CharSequence mTitle;
    
 
    
    // slide menu items
    private String[] navMenuTitles;
    private TypedArray navMenuIcons;
    private GMap my = new GMap();
    private LocationListener locationlistener;
    private List titles = new ArrayList <String>();
    private ArrayList<UEALocation> uealocation;

   // private UEALocationAdapter adapter;

    private UEALocationAdapter2 adapter;
    private LocationArray locArray;
    private GoogleMap gm;

    @SuppressWarnings("unchecked")
    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	setContentView(R.layout.activity_main);

	mTitle = mDrawerTitle = getTitle();

	// load slide menu items
	//navMenuTitles = getResources().getStringArray(R.array.nav_drawer_items);
	
	locArray = new LocationArray();
	LatLng latlng = new LatLng(52.621762999999990000, 1.240993000000003100);
	
	locArray.addLocation("Somewhere2", latlng);
	locArray.addLocation("Somewhere6", latlng);
	locArray.addLocation("Somewhere6", latlng);
	locArray.addLocation("Somewhere9", latlng);
	locArray.addLocation("Somewhere9", latlng);
	
	
	
	//my.getMap(gm, getCurrentFocus());
	GMap gmap = new GMap(getApplicationContext());
	gmap.getMap(gm);
	
	
	
	
	//the 
	int one = R.drawable.bullet_point;
	
	

	// both of these ids are within the activity_main.xml
	mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
	mDrawerList = (ListView) findViewById(R.id.list_slidermenu);

	//makes array list of uealocations
	uealocation = new ArrayList<UEALocation>();

	
	Log.e(TAG, "BEFORE");
	
	
	
	//iterates over the array of locations adding them as strings to be 
	//dynamicaly displayed
	
	

	mDrawerList.setOnItemClickListener(new SlideMenuClickListener());

	// setting the nav drawer list adapter
	adapter = new UEALocationAdapter2(getApplicationContext(), locArray, R.layout.drawer_list_item2);
	mDrawerList.setAdapter(adapter);

	// enabling action bar app icon and behaving it as toggle button
	getActionBar().setDisplayHomeAsUpEnabled(true);
	getActionBar().setHomeButtonEnabled(true);

	mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
		R.drawable.ic_drawer, // nav menu toggle icon
		R.string.app_name, // nav drawer open - description for
				   // accessibility
		R.string.app_name // nav drawer close - description for
				  // accessibility
	) {
	    public void onDrawerClosed(View view) {
		getActionBar().setTitle(mTitle);
		// calling onPrepareOptionsMenu() to show action bar icons
		invalidateOptionsMenu();
	    }

	    public void onDrawerOpened(View drawerView) {
		getActionBar().setTitle(mDrawerTitle);
		// calling onPrepareOptionsMenu() to hide action bar icons
		invalidateOptionsMenu();
	    }
	};
	mDrawerLayout.setDrawerListener(mDrawerToggle);
	getMap();

	// if (savedInstanceState == null) {
	// on first time display view for first nav item (homefragment)
	// refers to method below which appears to control everything!

	// displayView(0);
	// }
    }

    /**
     * Slide menu item click listener
     * */
    private class SlideMenuClickListener implements
	    ListView.OnItemClickListener {
	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
		long id) {
	    // display view for selected nav drawer item
	    // refers to the method below
	    displayView(position);
	}
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
	getMenuInflater().inflate(R.menu.main, menu);
	return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
	// toggle nav drawer on selecting action bar app icon/title
	if (mDrawerToggle.onOptionsItemSelected(item)) {
	    return true;
	}
	// Handle action bar actions click
	int id = item.getItemId();
	if (id == R.id.action_settings) {
		
			return true;
	}
	else {
			return super.onOptionsItemSelected(item);
	}
}

    /* *
     * Called when invalidateOptionsMenu() is triggered
     */
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
	// if nav drawer is opened, hide the action items
	boolean drawerOpen = mDrawerLayout.isDrawerOpen(mDrawerList);
	menu.findItem(R.id.action_settings).setVisible(!drawerOpen);
	return super.onPrepareOptionsMenu(menu);
    }

    /**
     * Displaying fragment view for selected nav drawer list item
     * */
    private void displayView(int position) {
	Log.e(TAG, "displayView");
	Toast.makeText(getApplicationContext(),
		"YOU PRESSED SOMETHING LOL ROFL", Toast.LENGTH_SHORT).show();
	

	@SuppressWarnings("unused")
	boolean marker = false;
	switch (position) {

	case 0:
	    Log.e(TAG, "case o");
	    my.getMap(gm);

	    if (marker = true) {
		gm.clear();
	    }

	    my.placeMarker(gm, 52.621762999999990000, 1.240993000000003100,
		    "String", true);
	    //my.findMe(gm);

	    marker = true;
	    setUpDrawer(0);

	    break;

	case 1:
	    if (marker = true) {
		gm.clear();
	    }

	    my.placeMarker(gm, 52.621762999999990000, 19.240993000000003100,
		    "String", true);

	    marker = true;
	    setUpDrawer(1);

	    break;

	case 2:
	    if (marker = true) {
		gm.clear();
	    }
	    my.placeMarker(gm, 52.621762999999990000, 10.240993000000003100,
		    "String", true);

	    marker = true;
	    setUpDrawer(2);

	    break;
	case 3:
	    if (marker = true) {
		gm.clear();
	    }
	    placeMarker(locArray.getLatLng(0),
		    "String");

	    marker = true;
	    setUpDrawer(3);

	    break;
	case 4:
	    if (marker = true) {
		gm.clear();
	    }

	    my.placeMarker(gm, 52.621762999999990000, 10.240993000000003100,
		    "String", true);

	    marker = true;
	    setUpDrawer(4);

	    break;
	case 5:
	    if (marker = true) {
		gm.clear();
	    }
	    my.placeMarker(gm, 50.621762999999990000, 1.240993000000003100,
		    "String", true);

	    marker = true;
	    setUpDrawer(5);
	    break;

	default:
	    break;
	}

    }

    /**
     * sets the title on the top bar as the title of the application
     */
    @Override
    public void setTitle(CharSequence title) {
	mTitle = title;
	getActionBar().setTitle(mTitle);
    }

    /**
     * When using the ActionBarDrawerToggle, you must call it during
     * onPostCreate() and onConfigurationChanged()...
     */

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
	super.onPostCreate(savedInstanceState);
	// Sync the toggle state after onRestoreInstanceState has occurred.
	mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
	super.onConfigurationChanged(newConfig);
	// Pass any configuration change to the drawer toggls
	mDrawerToggle.onConfigurationChanged(newConfig);
    }

    /**
     * Initilises the map by loaded the fragment.
     */
    private void initilizeMap() {
	if (gm == null) {
	    Log.e(TAG, "gm null");

	    gm = ((SupportMapFragment) getSupportFragmentManager()
		    .findFragmentById(R.id.map)).getMap();

	    // check if map is created successfully or not
	    if (gm == null) {
		Toast.makeText(getApplicationContext(),
			"Sorry! unable to create maps", Toast.LENGTH_SHORT)
			.show();
	    }
	}

    }

    /**
     * Private method to get the map. Calls on the initiliseMap method to laod
     * the map
     */
    private void getMap() {
	try {
		Log.e(TAG, "getMap");
	    // Loading map
	    initilizeMap();

	} catch (Exception e) {
	    e.printStackTrace();

	}

    }

    
    private void placeMarker2(double latitude, double longitude, String name) {

	gm.addMarker(new MarkerOptions().position(
		new LatLng(latitude, longitude)).title(name));
	CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(
		new LatLng(latitude, longitude), 10);
	gm.animateCamera(cameraUpdate);

    }
    
    private void placeMarker(LatLng latlng, String name) {

    	gm.addMarker(new MarkerOptions().position(
    		latlng).title(name));
    	CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(
    		latlng, 10);
    	gm.animateCamera(cameraUpdate);

        }

    /**
     * Closes the sliding menu and sets the title as being the title of the location
     * 
     * @param position
     */
    private void setUpDrawer(int position) {
	mDrawerLayout.closeDrawer(mDrawerList);
	setTitle(toString(position));
	
    }
    
    /**
     * Get the item from the array of location names as parses to a String 
     * to be used to set the title in the xml when the sliding drawer is opened and
     * that item at that position selected.
     * @param position
     * @return
     */
    private String toString (int position) {
	String title = (String) titles.remove(position);
	return title;
	
    }
    
    /**
     * this does nothing at the moment because I don't understand what I'm doing! :P
     * @author sofiaprice
     *
     */
    @SuppressWarnings("unused")
    private class MyLocationListener implements LocationListener {

	
	    @Override
	    public void onLocationChanged(Location loc) {
	        //editLocation.setText("");
	        //pb.setVisibility(View.INVISIBLE);
		Log.e(TAG, "LocationListener");
	        Toast.makeText(
	                getBaseContext(),
	                "Location changed: Lat: " + loc.getLatitude() + " Lng: "
	                    + loc.getLongitude(), Toast.LENGTH_SHORT).show();
	        String longitude = "Longitude: " + loc.getLongitude();
	        Log.e(TAG, longitude);
	        String latitude = "Latitude: " + loc.getLatitude();
	        Log.e(TAG, latitude);

	        /*------- To get city name from coordinates -------- */
	        String cityName = null;
	        Geocoder gcd = new Geocoder(getBaseContext(), Locale.getDefault());
	        List<Address> addresses;
	        try {
	            addresses = gcd.getFromLocation(loc.getLatitude(),
	                    loc.getLongitude(), 1);
	            if (addresses.size() > 0)
	                System.out.println(addresses.get(0).getLocality());
	            cityName = addresses.get(0).getLocality();
	        }
	        catch (IOException e) {
	            e.printStackTrace();
	        }
	        String s = longitude + "\n" + latitude + "\n\nMy Current City is: "
	            + cityName;
	        
	    }
	    
	    public void onProviderDisabled(String provider) {}

	    public void onProviderEnabled(String provider) {}

	    public void onStatusChanged(String provider, int status, Bundle extras) {}
	}
}


